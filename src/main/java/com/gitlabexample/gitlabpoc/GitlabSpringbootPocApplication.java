package com.gitlabexample.gitlabpoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabSpringbootPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabSpringbootPocApplication.class, args);
	}

}
